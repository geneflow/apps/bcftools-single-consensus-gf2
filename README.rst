BCFTools Consensus GeneFlow App
===============================

Version: 1.10.2-01

This GeneFlow app wraps the BCFTools Consensus tool to generate a consensus sequence for a single sample vcf. 

Inputs
------

1. input: Directory that contains a compressed VCF file and its index.

2. reference_sequence: Reference sequence fasta.

Parameters
----------

1. output: Output consensus sequence fasta.
